# README #

This is simple project using WebRTC technology create a video call.
Project using Google STUN server, you can setup your own server by this post:
**[My Github Page](http://dphans.github.io)**

# CONTACT #
* [http://dinophan.com/](http://dinophan.com/)
* [baophan94@icloud.com](mailto:baophan94@icloud.com)

### How do I get set up? ###

* Clone project into your workspace directory.
* Install dependencies.
* Start node server with app.js.
* Open **Chrome**, **Opera** browser and type address: http://localhost:2222.


### Browser supports ###

* Chrome.
* Opera.
* Chrome for Android.
* Opera for Android.
* Bower for iOS.

**!!! FIREFOX WILL COMMING SOON !!!**