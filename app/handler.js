/**
Question?
Feel free and contact me: baophan94@icloud.com
wWw: http://dinophan.com
**/

module.exports = function (socketIO, clients) {
	
	socketIO.on('connection', function (client) {
		
		console.log('*** EVENT: connection ***');
		clients.addClient(getRandomId(), client.id);
		client.emit('onId', clients.getClient(client.id));
		console.log('--> ADDED CLIENT: ' + JSON.stringify(clients.getClient(client.id)));
		console.log('CURRENT CLIENT(s): ' + clients.getAllClients().length);

		client.on('disconnect', function () {
			console.log('*** EVENT: disconnect ***');
			clients.removeClient(client.id);
			socketIO.sockets.emit('userIO', client.id);
			console.log('<-- CLIENT DISCONNECTED: ' + client.id);
			console.log('CURRENT CLIENT(s): ' + clients.getAllClients().length);
		});

		client.on('getSocketId', function (clientid) {
			console.log('*** EVENT: getSocketId ***');
			client.emit('onGetSocketId', clients.getSocketId(clientid));
			console.log('emitted: ' + clients.getSocketId(clientid));
		});

		client.on('message', function (details) {
			console.log('*** EVENT: message ***');
			var remote = socketIO.sockets.connected[details.to];
			if (!remote) {
				return;
			}
			delete details.to;
			details.from = client.id;
			remote.emit('message', details);
			console.log(JSON.stringify(details));
		});

	});

	/*==========  AppUtils  ==========*/
	function getRandomId () {
		return Math.floor(Math.random() * 900000) + 100000;
	}

};
