/**
Question?
Feel free and contact me: baophan94@icloud.com
wWw: http://dinophan.com
**/

module.exports = function () {

	var allClients = [];

	var Client = function (clientid, socketid) {
		this.clientid = clientid;
		this.socketid = socketid;
	};

	return {

		addClient: function (clientid, socketid) {
			var client = new Client(clientid, socketid);
			allClients.push(client);
		},

		removeClient: function (socketid) {
			var index = 0;
			while (index < allClients.length && allClients[index].socketid != socketid) {
				index++;
			}
			allClients.splice(index, 1);
		},

		updateClientId: function (clientid, socketid) {
			var client = allClients.find(function(element, i, array) {
				return element.socketid == socketid;
			});
			client.clientid = clientid;
		},

		getClient: function (socketid) {
			var index = 0;
			while(index < allClients.length && allClients[index].socketid != socketid){
				index++;
			}
			return allClients[index];
		},

		getAllClients: function () {
			return allClients;
		},

		getSocketId: function (clientid) {
			var index = 0;
			while(index < allClients.length && allClients[index].clientid != clientid) {
				index++;
			}
			if (!allClients[index]) {
				return null;
			}
			return allClients[index].socketid;
		}

	}

};
