/**
Question?
Feel free and contact me: baophan94@icloud.com
wWw: http://dinophan.com
**/


/*==========  SOCKET OBJECT  ==========*/
var socket 		= io();
var peers		= {};

/*==========  SOUND MANAGER  ==========*/
var SoundManager = function () {

	var SOUND_DAILING 	= "../sounds/dialing.mp3";
	var SOUND_CONNECTED = "../sounds/connected.wav";
	var SOUND_DISCONNECT= "../sounds/disconnected.wav";
	var SOUND_NEWMESSAGE= "../sounds/new_message.wav";

	var callingSound	= new Audio(SOUND_DAILING);
	var connectedSound	= new Audio(SOUND_CONNECTED);
	var disconnectedS	= new Audio(SOUND_DISCONNECT);
	var newMsgSound 	= new Audio(SOUND_NEWMESSAGE);

	callingSound.loop 	= true;
	connectedSound.loop = false;
	disconnectedS.loop	= false;
	newMsgSound.loop	= false;

	return {
		playDailingSound: function () {
			callingSound.play();
		},

		playConnectedSound: function () {
			callingSound.pause();
			connectedSound.play();
		},

		playDisconnectedSound: function () {
			callingSound.pause();
			disconnectedS.play();
		},

		playNewMessageSound: function () {
			newMsgSound.play();
		}
	};

};
var soundMan 	= new SoundManager();

// First depend of google, we have to create own server.
var servers 	= {
	"iceServers": [{
		"urls": "stun:stun.l.google.com:19302",
		"urls": "stun:stun1.l.google.com:19302",
		"urls": "stun:stun2.l.google.com:19302",
		"urls": "stun:stun3.l.google.com:19302",
		"urls": "stun:stun4.l.google.com:19302"
	}]
};

/* Config offer mandatory to callee */
var offerMandatory	= {
	'mandatory': {
		'OfferToReceiveAudio': true,
		'OfferToReceiveVideo': true
	}
};

/* Config local media stream */
var mediaConfig	= {
	audio: true,
	video: true
};


/* Setup default values */
$('#btnStartCall').hide();
$('#btnEndCall').hide();
$("#chatbox").hide();
$('#messageContainer').hide();

/*==========  LOCAL INFO  ==========*/
var localId			= "";
var localSocket		= "";
var localVideo		= document.getElementById('localVideo');
localVideo.volume	= 0;
var localStream;

var remoteId 		= "";
var remoteSocket	= "";
var remoteVideo 	= document.getElementById('remoteVideo');

var peerConnection;

/*==========  PEER CONNECTION OBJECT  ==========*/

// FOR GROUP CALL
var Peer = function (Id) {
	var pc 	= new webkitRTCPeerConnection(servers);
	pc.onicecandidate	= function (event) {
		if (!pc || !event || !event.candidate) return;
		var candidate = event.candidate;
		send(Id, 'candidate', {
			candidate: candidate.candidate,
			sdpMLineIndex: candidate.sdpMLineIndex
		});
	};
	pc.onaddstream		= function (event) {
		remoteVideo.src = window.URL.createObjectURL(event.stream);
		remoteVideo.onloadedmetadata = function (e) {
			remoteVideo.play();
			changeConnectedScreen();
			soundMan.playConnectedSound();
		};
	};
	pc.addStream(localStream);
};

var onicecandidate 	= function (event) {
	if (!peerConnection || !event || !event.candidate) return;
	var candidate = event.candidate;
	send(remoteSocket, 'candidate', {
		candidate: candidate.candidate,
		sdpMLineIndex: candidate.sdpMLineIndex
	});
};

var onaddstream		= function (event) {
	remoteVideo.src = window.URL.createObjectURL(event.stream);
	remoteVideo.onloadedmetadata = function (e) {
		remoteVideo.play();
		changeConnectedScreen();
		soundMan.playConnectedSound();
	};
};

function createNewPeerConnection () {
	peerConnection 					= new webkitRTCPeerConnection(servers);
	peerConnection.onicecandidate	= onicecandidate;
	peerConnection.onaddstream		= onaddstream;
	peerConnection.addStream(localStream);
}


/*==========  GET LOCAL STREAM  ==========*/
var onGetSuccess 	= function (ls) {
	localStream		=	ls;
	localVideo.src 	= window.URL.createObjectURL(localStream);
	localVideo.onloadedmetadata = function (e) {
		localVideo.play();
		createNewPeerConnection();
		changeWaitingForCallScreen();
	};
};

var onGetError		= function (errorDesc) {
	console.log("Browser doesn't support getUserMedia!");
};

navigator.getUserMedia 	= 	navigator.getUserMedia ||
							navigator.webkitGetUserMedia ||
							navigator.mozGetUserMedia;
if (navigator.getUserMedia) {
	navigator.getUserMedia(mediaConfig, onGetSuccess, onGetError);
}


/*==========  SOCKET EVENTS  ==========*/
socket.on('onId', function (details) {
	localId 	= details.clientid;
	localSocket	= details.socketid;
	$('#txtId').text('Your ID: ' + localId);
	console.log(JSON.stringify(details));
});

socket.on('onGetSocketId', function (socketId) {

	if (socketId == null) {
		bootbox.alert("User doesn't exist!", function() {
			soundMan.playDisconnectedSound();
		});
		return;
	}
	if (socketId == localSocket) {
		bootbox.alert("You can't call your-self!", function() {
			soundMan.playDisconnectedSound();
		});
		return;
	}

	remoteSocket = socketId;

	peerConnection.createOffer(function (sessionDescription) {
		peerConnection.setLocalDescription(sessionDescription);
		send(remoteSocket, 'offer', sessionDescription);
	}, logError, offerMandatory);

});

socket.on('userIO', function (userSocketId) {
	if (userSocketId === remoteSocket) {
		endCall();
	}
});

socket.on('message', function (details) {

	var from 	= details.from;
	var type 	= details.type;
	var payload	= details.payload;

	if (remoteSocket === '') {
		remoteSocket = from;
	}

	if (type === 'offer') {
		$('#txtId').text('Connecting...');
		peerConnection.setRemoteDescription(new RTCSessionDescription(payload));
		peerConnection.createAnswer(function (sessionDescription) {
			peerConnection.setLocalDescription(sessionDescription);
			send(details.from, 'answer', sessionDescription);
		}, logError, offerMandatory);
	}

	else if (type === 'answer') {
		peerConnection.setRemoteDescription(new RTCSessionDescription(payload));
	}
	
	else if (type === 'candidate') {
		peerConnection.addIceCandidate(new RTCIceCandidate({
			sdpMLineIndex: payload.sdpMLineIndex,
			candidate: payload.candidate
		}));
	}
	else if (type === 'bye') {
		remoteSocket = '';
		peerConnection.close();
		peerConnection 	= null;
		createNewPeerConnection();
		changeWaitingForCallScreen();
		soundMan.playDisconnectedSound();
	}
	else if (type === 'sms') {
		soundMan.playNewMessageSound();
		putMessageIntoConversation(payload, true);
	}
	else if (type === 'typing') {
		if (payload === true) {
			$('#typingStatus').text('(typing...)');
		}
		else {
			$('#typingStatus').text('');
		}
	}
});


/*==========  UTILS  ==========*/

var logError = function (errorDesc) {
	console.log('Err: ', errorDesc);
};

function changeConnectedScreen() {
	$("#localContainer").removeClass('localConnecting');
	$("#remoteContainer").removeClass('remoteConnecting');
	$('#localContainer').addClass('localConnected');
	$('#remoteContainer').addClass('remoteConnected');
	$('#btnStartCall').hide();
	$('#btnEndCall').show();
	$('#txtId').text('Incall with: ' + localId);
	clearMessage();
	$('#messageContainer').show(300);
}

function changeWaitingForCallScreen() {
	$("#localContainer").removeClass('localConnected');
	$("#remoteContainer").removeClass('remoteConnected');
	$('#localContainer').addClass('localConnecting');
	$('#remoteContainer').addClass('remoteConnecting');
	$('#btnStartCall').show();
	$('#btnEndCall').hide();
	$('#txtId').text('Your ID: ' + localId);
	clearMessage();
	$('#messageContainer').hide(300);
}


/*==========  USER ACTIONS  ==========*/
function startCall () {

	bootbox.prompt("Please enter your partner ID",
		function(result) {
			if (result != null) {                                             
				remoteId = result;
				socket.emit('getSocketId', remoteId);
				soundMan.playDailingSound();
				$('#txtId').text('Connecting... :")');
			}
		});
}

function endCall() {
	peerConnection.close();
	peerConnection 	= null;
	send(remoteSocket, 'bye', null);
	remoteSocket 	= '';
	createNewPeerConnection();
	changeWaitingForCallScreen();
	soundMan.playDisconnectedSound();
}

function send (to, type, payload) {
	socket.emit('message', {
		to: to,
		type: type,
		payload: payload
	});
}

/*==========  CHAT BOX  ==========*/
function toggleShowHideChatBox () {
	$("#chatbox").toggle(100);
	scrollToLastest();
}

function scrollToLastest () {
	var objDiv = document.getElementById("chatbody");
	objDiv.scrollTop = objDiv.scrollHeight;
}

function clearMessage () {
	$('#contentMessage').text('');
}

function sendMessage (messageText) {
	if (remoteSocket == '') {
		return;
	}
	messageText = strip(messageText);
	putMessageIntoConversation(messageText, false);
	send(remoteSocket, 'sms', messageText);
	$("#msgInput").val('');
}

function sendTypingStatus (isTyping) {
	send(remoteSocket, 'typing', isTyping);
}

function putMessageIntoConversation (msg, isRemote) {
	var messageWithStyle;
	if (isRemote) {
		messageWithStyle = '<li class="list-group-item"><i><b>partner:</b> ' + msg + '</i></li>';
	}
	else {
		messageWithStyle = '<li class="list-group-item"><b>you:</b> ' + msg + '</li>';
	}
	var lastMsg = $('#contentMessage').html();
	$('#contentMessage').html(lastMsg + messageWithStyle);
	scrollToLastest();
}

function strip (html) {
	var tmp = document.createElement("DIV");
	tmp.innerHTML = html;
	return tmp.textContent || tmp.innerText || "";
}

$("#msgInput").keypress(function(event) {
	var messageValue = $("#msgInput").val();
	if (messageValue != null) {
		if (event.keyCode === 13) {
			sendMessage(messageValue);
		}
	}
});

$("#msgInput").keyup(function(event) {
	var messageValue = $("#msgInput").val();
	if (messageValue === '') {
		sendTypingStatus(false);
	}
	else {
		sendTypingStatus(true);
	}
});

